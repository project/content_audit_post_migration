# Module flow
- There are two forms, one takes Source DB details and one takes Target DB details
- There is a FormHelper which embed common fields in the Form
- There is DbConnectionService
- There is a GenerateJsonFileInterface which has all the necessary methods to generate
- There are two controllers, one for generating the Source data & one for Target data
- There is a reportHelper which contains common information about the report
- There are two types of comparison reports, one for summary and one for details 
- First level comparison reports show summary i.e. count of data
- We are procrastinating the second level comparison reports