<?php

namespace Drupal\content_audit\Form;

use Drupal\Core\Form\FormBase;

use Drupal\Core\Form\FormStateInterface;

use Drupal\content_audit\SecondLevelReportBuilder;

class SecondLevelComparisonForm extends FormBase
{
    private $generateReportDisplay;
    public function __construct()
    {
        $this->generateReportDisplay = new SecondLevelReportBuilder;
    }

    public function getFormId()
    {
        return 'second_level_comparison';
    }
    public function buildForm(array $form, FormStateInterface $form_state)
    {
         $form['compare'] = [
            '#type' => 'select',
      '#title' => t('User'),
      '#description' => t('Select group members from the list.'),
      '#options' => array(
         0 => t('Anonymous'),
         1 => t('Admin'),
         2 => t('foobar'),
         // This should be implemented in a better way!
       ),
         ];

         return $form;

    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
    }
}
