<?php

namespace Drupal\content_audit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\content_audit\Helpers\FormHelpers;

/**
 * Class SourceConfigForm.
 */
class SourceConfigForm extends ConfigFormBase
{
    /**
     * getFormId.
     *
     * @return string
     */
    public function getFormId()
    {
        return 'content_audit.source_settings';
    }

    /**
     * buildForm.
     *
     * @param array form
     * @param FormStateInterface form_state
     *
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $formHelper = new FormHelpers;
        $form [] = $formHelper->createDbFields('content_audit.source_settings');
        return parent::buildForm($form, $form_state);
    }

    /**
     * submitForm.
     *
     * @param array form
     * @param FormStateInterface form_state
     *
     * @return void
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->config('content_audit.source_settings');

        $formHelper = new FormHelpers;
        $formHelper->submitConfiguration($form, $form_state, $config);
        $config->save();
        
        $formHelper->testDbConnection('content_audit.source_settings', 'source');
        return parent::submitForm($form, $form_state);
    }

    /**
     * getEditableConfigNames.
     *
     * @return void
     */
    protected function getEditableConfigNames()
    {
        return [
            'content_audit.source_settings',
        ];
    }
}
