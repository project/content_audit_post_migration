<?php

namespace Drupal\content_audit\Form;

use Drupal\Core\Form\FormBase;

use Drupal\Core\Form\FormStateInterface;

use Drupal\content_audit\FirstLevelReportBuilder;

class FirstLevelComparisonForm extends FormBase
{
    private $generateReportDisplay;
    public function __construct()
    {
        $this->generateReportDisplay = new FirstLevelReportBuilder;
    }

    public function getFormId()
    {
        return 'first_level_comparison';
    }
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        return $this->generateReportDisplay->generateTabularReport();
       



        //return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
    }
}
