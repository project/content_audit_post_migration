<?php

namespace Drupal\content_audit;

/**
 * AN interface to generate JSON file for source & destination in following format
 * Platoform:
 * TimeStamp:
 * URL:
 * CountOfNodeTypes: getTotalCountOfNodeTypes()
 * CountOfEckEntityTypes: getTotalCountOfEckEntityTypes()
 * CountOfTaxonomyVocabs: getTotalCountOfTaxonomyVocalbularies()
 * CountOfCustomNodeFields: getTotalCountOfCustomNodeFields()
 * CountOfCustomUserFields: getTotalCountOfCustomUserFields()
 * CountOfTaxonomyFields: getTotalCountOfTaxonomyFields() // task for later stage
 * CountOfUrlAliases: getTotalCountOfUrlAliases()
 * CountOfUrlRedirects: getTotalCountOfUrlRedirects()
 * CountOfMainNavItems: getTotalCountOfMainNavItems()
 *
 * UsersData:
 *   UniqueField: uniqueFields
 *   Fields: getUserFields()
 *   Count: getTotalCountOfUsers()
 *   UserIdsWithTitle: getUsersIdsWithTitle()
 * TaxonomyData:
 *  VocabName: getTaxonomyVocalbularies()
 *    Vocab1:
 *          UniqueField: uniqueFields
 *          Fields: getTaxonomyVocalbulariesFields($vocab)
 *          Count: getTaxonomyVocalbulariesTermsCount($vocab)
 *          TaxoIdsWithTitle: getTaxonomyTermIdsWithTitle($vocab)
 *    Vocab2:
 *          UniqueField: uniqueFields
 *          Fields: getTaxonomyVocalbulariesFields($vocab)
 *          Count: getTaxonomyVocalbulariesTermsCount($vocab)
 *          TaxoIdsWithTitle: getTaxonomyTermIdsWithTitle($vocab)
 *  NodeData:
 *  NodeType: getNodeTypes()
 *    Node1:
 *          UniqueField: uniqueFields
 *          Fields: getNodeFields($nodeType)
 *          Count: getNodeCount($nodeType)
 *          NodeIdsWithTitle: getNodeIdsWithTitle($nodeType)
 *    Node2:
 *          UniqueField: uniqueFields
 *          Fields: getNodeFields($nodeType)
 *          Count: getNodeCount($nodeType)
 *          NodeIdsWithTitle: getNodeIdsWithTitle($nodeType)
 *
 */
interface GenerateJsonFileInterface
{
    const uniqueFields = 'id/title';
    /**
     * Create unique file name SiteVersion-DDMMYYHis.json e.g. D7-200622011001.json
     */
    // Task for later stage public function setFileName(string $version);

    /**
     * create file under defined path
     */
    // Task for later stage public function createFile($path);

    /**
     * Write JSON headers
     * Platform: D7/D9
     * Timestamp: DDMMYYHis
     * URL: https://abc.com
     */
    // task for later stage public function writeJsonHeader($version, $time, $siteUrl);

    /* Get Counts of Entity types */
    public function getTotalCountOfNodeTypes();

    // Task for later stager public function getTotalCountOfEckEntityTypes();

    public function getTotalCountOfTaxonomyVocalbularies();

    /** Get Count of build fields */
    public function getTotalCountOfCustomNodeFields();

    public function getTotalCountOfCustomUserFields();

    // task for later stage public function getTotalCountOfTaxonomyFields();

    /** Get Count of URLs */
    public function getTotalCountOfUrlAliases();

    public function getTotalCountOfUrlRedirects();

    /** Get count of main nav items */
    public function getTotalCountOfMainNavItems();

    /** UsersData */
    public function getUserFields();

    public function getTotalCountOfUsers();

    // public function getTotalCountOfUserRoles();

    public function getUsersIdsWithTitleAndRoles();



    /** TaxonomyData */
    public function getTaxonomyVocalbularies();

    public function getTotalCountOfTaxonomyTerms();

    public function getTaxonomyTermIdsWithTitle();

    // task for later stage public function getTaxonomyVocalbulariesFields($vocab);



    /** NodeData */

    public function getNodeTypes();

    public function getNodeFields();

    public function getTotalNodeCount();

    public function getCtWiseNodeCount();

    public function getCtWiseNodeIdsWithTitle();

    /** File Data */
    public function getTotalFileCount();
}
