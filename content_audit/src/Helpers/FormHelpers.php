<?php

namespace Drupal\content_audit\Helpers;

class FormHelpers
{
    public function createDbFields($settingsName)
    {
        $config = \Drupal::config($settingsName);
        $form['database'] = [
            '#type' => 'textfield',
            '#length' => '100',
            '#title' => 'Database Name',
            '#default_value' => $config->get('database'),
            '#required' => true,
        ];
        $form['host'] = [
            '#type' => 'textfield',
            '#length' => '100',
            '#title' => 'Host',
            '#default_value' => $config->get('host'),
            '#required' => true,
        ];
        $form['username'] = [
            '#type' => 'textfield',
            '#length' => '100',
            '#title' => 'username',
            '#default_value' => $config->get('username'),
            '#required' => true,
        ];
        $form['password'] = [
            '#type' => 'textfield',
            '#length' => '200',
            '#title' => 'Password',
            '#default_value' => $config->get('password'),
            '#required' => true,
        ];
        $form['port'] = [
            '#type' => 'textfield',
            '#length' => '5',
            '#title' => 'Port',
            '#default_value' => $config->get('port'),
            '#required' => true,
        ];
        $form['driver'] = [
            '#type' => 'textfield',
            '#length' => '10',
            '#title' => 'Driver',
            '#default_value' => $config->get('driver'),
            '#required' => true,
        ];
        return $form;
    }

    public function submitConfiguration($form, $form_state, $config)
    {
        $config->set('database', $form_state->getValue('database'));
        $config->set('username', $form_state->getValue('username'));
        $config->set('password', $form_state->getValue('password'));
        $config->set('host', $form_state->getValue('host'));
        $config->set('port', $form_state->getValue('port'));
        $config->set('driver', $form_state->getValue('driver'));
    }

    public function testDbConnection($settingsName, $type)
    {
        $canConnectToDb = \Drupal::service('content_audit.db_connection')->siteDbConnection($settingsName, $type);
        $message = \Drupal::service('messenger');
        if ($canConnectToDb) {
            $status = $message->addStatus('Can connect to DB successfully');
        } else {
            $status = $message->addError('There was an issue connecting to DB');
        }

        return $status;
    }
}
