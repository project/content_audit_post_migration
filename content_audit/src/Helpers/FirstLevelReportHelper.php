<?php

namespace Drupal\content_audit\Helpers;

use Drupal\content_audit\Helpers\ReportHelpers;

class FirstLevelReportHelper extends ReportHelpers
{
    /***
     * First Level Comparison--3 of 16
     */
    public function createMarkUpForNodeTypeCount($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Content Types:Count', 'getTotalCountOfNodeTypes', 1);
    }



    public function createMarkUpForTotalCountOfTaxonomyVocalbularies($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Taxonomy Vocalbularies:Count', 'getTotalCountOfTaxonomyVocalbularies', 1);
    }

    public function createMarkUpForTotalCountOfCustomNodeFields($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Node Custom Fields:Count', 'getTotalCountOfCustomNodeFields', 1);
    }

    public function createMarkUpForTotalCountOfCustomUserFields($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'User Custom Fields:Count', 'getTotalCountOfCustomUserFields', 1);
    }

    public function createMarkUpForTotalCountOfUrlAliases($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'URL Aliases:Count', 'getTotalCountOfUrlAliases', 1);
    }

    public function createMarkUpForTotalCountOfUrlRedirects($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'URL Redirects:Count', 'getTotalCountOfUrlRedirects', 1);
    }

    public function createMarkUpForTotalCountOfMainNavItems($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Main Nav Items:Count', 'getTotalCountOfMainNavItems', 1);
    }

    public function createMarkUpForUserFields($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'User Custom Fields', 'getUserFields', 1);
    }

    public function createMarkUpForTotalCountOfUsers($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Users:Count', 'getTotalCountOfUsers', 1);
    }

    public function createMarkUpForTaxonomyVocalbularies($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Taxonomy Vocabularies', 'getTaxonomyVocalbularies', 1);
    }

    public function createMarkUpForNodeTypes($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Content Types', 'getNodeTypes', 1);
    }

    public function createMarkUpForNodeFields($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Node Custom Fields', 'getNodeFields', 1);
    }

    public function createMarkUpForTotalNodeCount($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Total Nodes:Count', 'getTotalNodeCount', 1);
    }

    public function createMarkUpForCtWiseNodeCount($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'CT Wise Node::Count', 'getCtWiseNodeCount', 1);
    }

    public function createMarkUpForTotalFileCount($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Files:Count', 'getTotalFileCount', 1);
    }

    public function createMarkUpForUsersIdsWithTitleAndRoles($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'User IDs with title and roles', 'getUsersIdsWithTitleAndRoles', 1);
    }


    public function createMarkUpForTaxonomyTermIdsWithTitle($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Term IDs with title', 'getTaxonomyTermIdsWithTitle', 1);
    }

    public function createMarkUpForTotalCountOfUserRoles($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Roles:Count', 'getTotalCountOfUserRoles', 1);
    }

    public function createMarkUpForTotalCountOfTaxonomyTerms($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Taxonomy Terms:Count', 'getTotalCountOfTaxonomyTerms', 1);
    }
}
