<?php

namespace Drupal\content_audit\Helpers;

use Drupal\content_audit\Controller\SourceJsonDataController;
use Drupal\content_audit\Controller\TargetJsonDataController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;

class ReportHelpers
{
    public $dbObject;
    private $sourceData;
    private $targetData;
    public function __construct()
    {
        $this->dbObject = \Drupal::service('content_audit.db_connection');
        $this->sourceData = new SourceJsonDataController($this->dbObject);
        $this->targetData = new TargetJsonDataController($this->dbObject);
    }

    protected function setMarkUpData($counter, $title, $functionToCall, $level)
    {
        $sourceValue = $this->sourceData->$functionToCall();
        $targetValue = $this->targetData->$functionToCall();
       
        return [
            $counter,
            $this->checkMatchStatus($sourceValue, $targetValue, $level),
            $this->displayActionButton($level),
           $this->displayRemarks($sourceValue, $targetValue, $level),
            $title,
           $sourceValue,
            $targetValue,
            
        ];
    }

    private function checkMatchStatus($sourceValue, $targetValue, int $level)
    {
        $isFirstLevel = $level == 1;
        if ($isFirstLevel) {
            $isSourceAndTargetEquals = $sourceValue == $targetValue;
            return $isSourceAndTargetEquals == 1 ? Markup::create('<span class="system-status-counter__status-icon system-status-counter__status-icon--checked"></span>') : Markup::create('<span class="system-status-counter__status-icon system-status-counter__status-icon--error"></span>');
        } else {
            return 'Second Level'; // ToDo
        }
    }

    private function displayActionButton(int $level)
    {
        $isFirstLevel = $level == 1;
        if ($isFirstLevel) {
            return 'Processed';
        } else {
            return 'Second Level';
        }
    }

    private function displayRemarks($sourceValue, $targetValue, int $level)
    {
        $isFirstLevel = $level == 1;
        if ($isFirstLevel) {
            $isSourceAndTargetEquals = $sourceValue == $targetValue;
            return $isSourceAndTargetEquals == 1 ? 'No' : $this->getDifference($sourceValue, $targetValue);
        } else {
            return 'Second Level'; // ToDo
        }
    }

    public function getDifference($from, $to)
    {
        if (intval(strip_tags($from)) > 0 && intval(strip_tags($to)) > 0) {
            $diffBwSourceAndTarget = intval(strip_tags($from)) - intval(strip_tags($to));
            return $diffBwSourceAndTarget > 0 ? $diffBwSourceAndTarget.' row (s) are not in target': ($diffBwSourceAndTarget * -1).' additional row (s) in target' ;
        } else {
            $output ='';
            $fromArray = explode('<br />', $from);
            $toArray = explode('<br />', $to);
            $diff1 = array_diff($fromArray, $toArray);
            $diff2 = array_diff($toArray, $fromArray);
        
            if (count($diff1) > 0) {
                $output = join(',', $diff1).' not in <b>Target</b>'."\n";
            }
            if (count($diff2) > 0) {
                $output = join(',', $diff2).' not in <b>Source</b>';
            }

            if (count($diff1) > 0 && count($diff2) > 0) {
                $output = join(',', $diff1).' not in <b>Target</b> '."\n".join(',', $diff2).' not in <b>Source</b>';
            }
            return Markup::create(nl2br($output));
        }
    }
}
