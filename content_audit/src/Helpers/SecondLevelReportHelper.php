<?php

namespace Drupal\content_audit\Helpers;

use Drupal\content_audit\Helpers\ReportHelpers;

class SecondLevelReportHelper extends ReportHelpers
{
    /***
     * Second Level Comparison
     */
    public function createMarkUpForCtWiseNodeIdsWithTitle($rowNo)
    {
        return $this->setMarkUpData($rowNo, 'Content Types:Count', 'getCtWiseNodeIdsWithTitle', 1);
    }
}
