<?php

namespace Drupal\content_audit\Services;

use Drupal\Core\Database\Database;

class DbConnectionService
{
    public static function siteDbConnection($settings, $type)
    {
        $config = \Drupal::config($settings);

        $database = [
            'database' => $config->get('database'),
            'username' => $config->get('username'),
            'password' => $config->get('password'),
            'host' => $config->get('host'),
            'driver' => $config->get('driver'),
            'port' => $config->get('port'),
            'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
            'PDO' => array(\PDO::ATTR_TIMEOUT => 10.0, \PDO::MYSQL_ATTR_COMPRESS => 1)
        ];

        Database::addConnectionInfo($type, 'default', $database);
        Database::setActiveConnection($type);
        try {
            $siteDbConnection = Database::getConnection();
            return $siteDbConnection;
        } catch (\Exception $e) {
            $message = $e->getMessage();
            \Drupal::messenger()->addError($message);
            return false;
        }
    }
}
