<?php

namespace Drupal\content_audit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\content_audit\GenerateJsonFileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;

class SourceJsonDataController extends ControllerBase implements GenerateJsonFileInterface
{
    private $dbSource;

    public function __construct($dbSource)
    {
        $this->dbSource = $dbSource->siteDbConnection('content_audit.source_settings', 'source');
    }

    public static function create(ContainerInterface $container)
    {
        $dbSource = $container->get('content_audit.db_connection');
        return new static($dbSource);
    }

    /**
     * createSourceJsonFile
     *
     * @return array
     */
    public function createSourceJsonFile()
    {
        return [
            '#markup' => $this->getCtWiseNodeIdsWithTitle()
        ];
    }

    public function getTotalCountOfNodeTypes()
    {
        $result = $this->dbSource->select('node', 'n')
            ->fields('n', ['type'])
            ->distinct()
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfEckEntityTypes()
    {


        // To DO
    }

    public function getTotalCountOfTaxonomyVocalbularies()
    {
        $result = $this->dbSource->select('taxonomy_term_data', 't')
            ->fields('t', ['vid'])
            ->distinct()
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfCustomNodeFields()
    {
        $result = $this->dbSource->select('field_config_instance', 'f')
            ->fields('f', ['id'])
            ->condition('entity_type', 'node', '=')
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfCustomUserFields()
    {
        $result = $this->dbSource->query('SHOW TABLES LIKE \'user__field_%\'')
            ->fetchAll();
        return count($result);
    }



    /** Get Count of URLs */
    public function getTotalCountOfUrlAliases()
    {
        $result = $this->dbSource->select('url_alias', 'u')
            ->fields('u', ['pid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfUrlRedirects()
    {
        $result = $this->dbSource->select('redirect', 'r')
            ->fields('r', ['id'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    /** Get count of main nav items */
    public function getTotalCountOfMainNavItems()
    {
        $result = $this->dbSource->select('menu_links', 'm')
            ->fields('m', ['menu_name'])
            ->condition('menu_name', 'main-menu', '=')
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    /** UsersData */
    public function getUserFields()
    {
        $result = $this->dbSource->query('SHOW TABLES LIKE \'user__field_%\'')
            ->fetchCol();
        $fields = '';
        foreach ($result as $item) {
            $fields .= str_replace('user__', '', $item) . "\n";
        }
        return Markup::create(nl2br($fields));
    }

    public function getTotalCountOfUsers()
    {
        $result = $this->dbSource->select('users', 'u')
            ->fields('u', ['uid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getUsersIdsWithTitleAndRoles()
    {
        $result = $this->dbSource->query('SELECT u.uid AS uid, u.name AS name, ur.rid AS rid,r.name AS RNAME FROM users u LEFT OUTER JOIN users_roles ur ON u.uid = ur.uid LEFT OUTER JOIN role r ON ur.rid = r.rid')
            ->fetchAll();
        $usersData = '';
        foreach ($result as $object) {
            $usersData .= $object->uid . '-' . $object->name . '-' . $object->RNAME . "\n";
        }
        return Markup::create(nl2br($usersData));
    }

    public function getTotalCountOfUserRoles()
    {
        $result = $this->dbSource->select('role', 'r')
            ->fields('u', ['rid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    /** TaxonomyData */
    public function getTaxonomyVocalbularies()
    {
        $result = $this->dbSource->select('taxonomy_vocabulary', 't')
            ->fields('t', ['machine_name'])
            ->execute()
            ->fetchAll();
        $taxonomyData = '';
        foreach ($result as $object) {
            $taxonomyData .= $object->machine_name . "\n";
        }
        return Markup::create(nl2br($taxonomyData));
    }

    /**
     * getTotalCountOfTaxonomyTerms
     *
     * @return int
     */
    public function getTotalCountOfTaxonomyTerms(): int
    {
        $result = $this->dbSource->select('taxonomy_term_data', 't')
            ->fields('t', ['tid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTaxonomyTermIdsWithTitle()
    {
        $fetchTaxoData = $this->dbSource->select('taxonomy_term_data', 't')
            ->fields('t', ['tid', 'name'])
            ->orderBy('tid', 'ASC')
            ->execute()
            ->fetchAll();
        $output = '';
        foreach ($fetchTaxoData as $object) {
            $output .= $object->tid . '-' . $object->name . "\n";
        }

        return Markup::create(nl2br($output));
    }

    /** NodeData */

    public function getNodeTypes()
    {
        $result = $this->dbSource->select('node', 'n')
            ->fields('n', ['type'])
            ->distinct()
            ->orderBy('type', 'ASC')
            ->execute()

            ->fetchAll();
        $nodeData = '';
        foreach ($result as $object) {
            $nodeData .= $object->type . "\n ";
        }
        return Markup::create(nl2br($nodeData));
    }

    public function getNodeFields()
    {
        /**
         *
         * Check all field names prefix with field_ having node entity type in field_config_instance
         *
         */
        $result = $this->dbSource->query('SELECT field_name AS FNAME,COUNT(field_name) AS UCOUNT,GROUP_CONCAT(bundle) AS BUNDLE FROM field_config_instance WHERE entity_type=\'node\' GROUP BY field_name ORDER BY field_name')
            ->fetchAll();

        $output = '';
        foreach ($result as $object) {
            $output .= $object->FNAME . ' is being used in ' . $object->UCOUNT . ' place(s) ' . $object->BUNDLE . ' CT(s) ' . "\n";
        }
        return Markup::create(nl2br($output));
    }

    public function getTotalNodeCount()
    {
        $result = $this->dbSource->select('node', 'n')
            ->fields('n', ['nid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getCtWiseNodeCount()
    {
        $output = '';
        $result = $this->dbSource->query('SELECT type AS nodeType,COUNT(type) AS `count` FROM node GROUP BY TYPE')
            ->fetchAll();
        foreach ($result as $object) {
            $output .= $object->nodeType . ' have ' . $object->count . ' node(s)' . "\n";
        }
        return Markup::create(nl2br($output));
    }

    public function getCtWiseNodeIdsWithTitle()
    {
        $nodeTypes = $this->getNodeTypes();
        $cleanNodeTypes = str_replace('<br>', ',', $nodeTypes);
        $nodeTypesForQuery = explode(',', rtrim($cleanNodeTypes, ','));
        $output = '';
        foreach ($nodeTypesForQuery as $nodeType) {
            $nodeData = '';
            $fetchNodeData = $this->dbSource->select('node_field_data', 'n')
                ->fields('n', ['nid', 'title'])
                ->condition('type', $nodeType, '=')
                ->execute()
                ->fetchAll();

            foreach ($fetchNodeData as $object) {
                $nodeData .= $object->nid . '-' . $object->title . '<br>';
            }
            $output .= '<b>' . $nodeType . '[' . count($fetchNodeData) . ']</b><br>' . $nodeData;
        }
        return $output;
    }

    /** File Data */
    public function getTotalFileCount()
    {
        $result = $this->dbSource->select('file_managed', 'f')
            ->fields('f', ['id'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }
}
