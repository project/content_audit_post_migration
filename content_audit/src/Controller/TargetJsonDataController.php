<?php

namespace Drupal\content_audit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\content_audit\GenerateJsonFileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Markup;

class TargetJsonDataController extends ControllerBase implements GenerateJsonFileInterface
{
    private $dbTarget;

    public function __construct($dbTarget)
    {
        $this->dbTarget = $dbTarget->siteDbConnection('content_audit.target_settings', 'target');
    }

    public static function create(ContainerInterface $container)
    {
        $dbTarget = $container->get('content_audit.db_connection');
        return new static($dbTarget);
    }

    public function createTargetJsonFile()
    {
        return [
            '#markup' => $this->getCtWiseNodeIdsWithTitle()
        ];
    }

    /**
     * First Level Comparison
     */
    public function getTotalCountOfNodeTypes()
    {
        $result = $this->dbTarget->select('node', 'n')
            ->fields('n', ['type'])
            ->distinct()
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfEckEntityTypes()
    {


        // To DO
    }

    public function getTotalCountOfTaxonomyVocalbularies()
    {
        $result = $this->dbTarget->select('taxonomy_term_data', 't')
            ->fields('t', ['vid'])
            ->distinct()
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfTaxonomyTerms():int
    {
        $result = $this->dbTarget->select('taxonomy_term_field_data', 't')
            ->fields('t', ['tid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfCustomNodeFields()
    {
        $result = $this->dbTarget->query('SHOW TABLES LIKE \'node__field_%\'')
        ->fetchAll();
        return count($result);
    }

    public function getTotalCountOfCustomUserFields()
    {
        $result = $this->dbTarget->query('SHOW TABLES LIKE \'user__field_%\'')
            ->fetchAll();
        return count($result);
    }



    /** Get Count of URLs */
    public function getTotalCountOfUrlAliases()
    {
        $result = $this->dbTarget->select('path_alias', 'p')
            ->fields('p', ['id'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getTotalCountOfUrlRedirects()
    {
        $result = $this->dbTarget->select('redirect', 'r')
            ->fields('r', ['id'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    /** Get count of main nav items */
    public function getTotalCountOfMainNavItems()
    {
        $result = $this->dbTarget->select('menu_tree', 'm')
            ->fields('m', ['menu_name'])
            ->condition('menu_name', 'main', '=')
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    /** UsersData */
    public function getUserFields()
    {
        $result = $this->dbTarget->query('SHOW TABLES LIKE \'user__field_%\'')
            ->fetchCol();
        $fields = '';
        foreach ($result as $item) {
            $fields .= str_replace('user__', '', $item)."\n";
        }
        return Markup::create(nl2br($fields));
    }

    public function getTotalCountOfUsers()
    {
        $result = $this->dbTarget->select('users', 'u')
            ->fields('u', ['id'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }



    /** TaxonomyData */
    public function getTaxonomyVocalbularies()
    {
        $result = $this->dbTarget->select('taxonomy_term_data', 't')
            ->fields('t', ['vid'])
            ->distinct()
            ->execute()
            ->fetchAll();
        $taxonomyData = '';
        foreach ($result as $object) {
            $taxonomyData .= $object->vid."\n";
        }
        return Markup::create(nl2br($taxonomyData));
    }



    /** NodeData */

    public function getNodeTypes()
    {
        $result = $this->dbTarget->select('node', 'n')
            ->fields('n', ['type'])
            ->distinct()
            ->execute()
            ->fetchAll();
        $nodeData = '';
        foreach ($result as $object) {
            $nodeData .= $object->type."\n ";
        }
        return Markup::create(nl2br($nodeData));
    }

    public function getNodeFields()
    {
        /**
         *
         * look in every table prefixed with node__field%--yes
         * check bundle column == node type searching for--don't match group all bundles
         * if it's > 1 it's a field for that bundle--no
         * show bundlewise fields
         */
        $output = '';
        $resultBody = $this->dbTarget->query('SELECT GROUP_CONCAT(DISTINCT bundle) AS BUNDLE,count(DISTINCT bundle) AS UCOUNT from node__body')
            ->fetchAll();

        $output = '';
        foreach ($resultBody as $objectBody) {
            $output .= 'body is being used in '.$objectBody->UCOUNT.' place(s) '.$objectBody->BUNDLE.' CT(s) '."\n";
        }

        $result = $this->dbTarget->query('SHOW TABLES LIKE \'node__field_%\'')
            ->fetchCol();


        foreach ($result as $tableName) {
            $bundles = '';
            $searchNodeType = $this->dbTarget->select($tableName, 'n')
                  ->fields('n', ['bundle'])
                  ->distinct()
                  ->execute()
                  ->fetchCol();
            foreach ($searchNodeType as $bundleName) {
                $bundles .= $bundleName.',';
            }
            $output .= str_replace('node__', '', $tableName).' is being used in '.count($searchNodeType).' place(s) '.rtrim($bundles, ',').' CT(s)'."\n";
        }
        return Markup::create(nl2br($output));
    }

    public function getTotalNodeCount()
    {
        $result = $this->dbTarget->select('node', 'n')
            ->fields('n', ['nid'])
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }

    public function getCtWiseNodeCount()
    {
        $output = '';
        $result = $this->dbTarget->query('SELECT type AS nodeType,COUNT(type) AS `count` FROM node GROUP BY TYPE')
               ->fetchAll();
        foreach ($result as $object) {
            $output .= $object->nodeType.' have '.$object->count.' node(s)'."\n";
        }
        return Markup::create(nl2br($output));
    }



    /** File Data */
    public function getTotalFileCount()
    {
        $result = $this->dbTarget->select('file_managed', 'f')
                ->fields('f', ['id'])
                ->countQuery()
                ->execute()
                ->fetchField();
        return $result;
    }

    /**
     * Second Level Comparison
     */

    public function getCtWiseNodeIdsWithTitle()
    {
        $nodeTypes = $this->getNodeTypes();
        $cleanNodeTypes = str_replace('<br>', ',', $nodeTypes);
        $nodeTypesForQuery = explode(',', rtrim($cleanNodeTypes, ','));
        $output = '';
        foreach ($nodeTypesForQuery as $nodeType) {
            $nodeData = '';
            $fetchNodeData = $this->dbTarget->select('node_field_data', 'n')
                    ->fields('n', ['nid','title'])
                    ->condition('type', $nodeType, '=')
                    ->execute()
                    ->fetchAll();

            foreach ($fetchNodeData as $object) {
                $nodeData .= $object->nid.'-'.$object->title.'<br>';
            }
            $output .= '<b>'.$nodeType.'['.count($fetchNodeData).']</b><br>'.$nodeData;
        }
        return $output;
    }

    public function getTaxonomyTermIdsWithTitle()
    {
        $output = '';

        $fetchTaxoData = $this->dbTarget->select('taxonomy_term_field_data', 't')
                    ->fields('t', ['tid','name'])
                    ->orderBy('tid', 'ASC')
                    ->execute()

                    ->fetchAll();

        foreach ($fetchTaxoData as $object) {
            $output .= $object->tid.'-'.$object->name."\n";
        }

        return Markup::create(nl2br($output));
    }

    public function getUsersIdsWithTitleAndRoles()
    {
        $result = $this->dbTarget->query('SELECT u.uid AS uid, u.name AS name, ur.roles_target_id AS RNAME FROM users_field_data u LEFT OUTER JOIN user__roles ur ON u.uid = ur.entity_id')
            ->fetchAll();
        $usersData = '';
        foreach ($result as $object) {
            $usersData .= $object->uid.'-'.$object->name.'-'.$object->RNAME."\n";
        }
        return Markup::create(nl2br($usersData));
    }

    public function getTotalCountOfUserRoles()
    {
        $result = $this->dbTarget->select('user__roles', 'r')
            ->fields('r', ['roles_target_id'])
            ->distinct()
            ->countQuery()
            ->execute()
            ->fetchField();
        return $result;
    }
}
