<?php

namespace Drupal\content_audit;

use Drupal\Core\Render\Markup;
use Drupal\content_audit\Helpers\SecondLevelReportHelper;

class SecondLevelReportBuilder extends SecondLevelReportHelper
{
    public function generateTabularReport()
    {
        $build = $this->buildTable();
        return [
            '#type' => '#markup',
            '#markup' => render($build)
          ];
    }

    public function setHeader()
    {
        return $header = [
            'SNo' => t('SNo'),
            'Particular' => t('Particular'),
            'Source' => t('Source'),
            'Target' => t('Target'),
            'Action' => t('Action'),
            'Status' => t('Status'),
            'More Info'  => t('More Info'),
          ];
    }

    public function generateTableRows()
    {
        $row = 0;
        return $rows = [
           // $this->createMarkUpForCtWiseNodeIdsWithTitle(++$row)

          ];
    }

    public function buildTable()
    {
        return $build['table'] = [
            '#type' => 'table',
            '#header' => $this->setHeader(),
            '#rows' => $this->generateTableRows(),
            '#empty' => t('Sorry, nothing to display.'),
          ];
    }
}
