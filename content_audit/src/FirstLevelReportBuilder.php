<?php

namespace Drupal\content_audit;

use Drupal\Core\Render\Markup;
use Drupal\content_audit\Helpers\FirstLevelReportHelper;

class FirstLevelReportBuilder extends FirstLevelReportHelper
{
    public function generateTabularReport()
    {
        $build = $this->buildTable();
        return [
            '#type' => '#markup',
            '#markup' => render($build)
          ];
    }

    public function setHeader()
    {
        return $header = [
            'SNo' => t('SNo'),
            'Status' => t('Status'),
            'Action' => t('Action'),

            'More Info'  => t('More Info'),
            'Particular' => t('Particular'),
            'Source' => t('Source'),
            'Target' => t('Target'),

          ];
    }

    public function generateTableRows()
    {
        $row = 0;
        return $rows = [
           $this->createMarkUpForTotalCountOfUsers(++$row),
          // $this->createMarkUpForUsersIdsWithTitleAndRoles(++$row),
            $this->createMarkUpForTotalCountOfCustomUserFields(++$row),
         // $this->createMarkUpForUserFields(++$row),

          $this->createMarkUpForTotalCountOfUserRoles(++$row),

            $this->createMarkUpForTotalCountOfTaxonomyVocalbularies(++$row),
            $this->createMarkUpForTotalCountOfTaxonomyTerms(++$row),
          //  $this->createMarkUpForTaxonomyVocalbularies(++$row),
          //$this->createMarkUpForTaxonomyTermIdsWithTitle(++$row),

           $this->createMarkUpForNodeTypeCount(++$row),
           //$this->createMarkUpForTotalCountOfCustomNodeFields(++$row),
           //$this->createMarkUpForNodeTypes(++$row),
           // $this->createMarkUpForNodeFields(++$row),
           $this->createMarkUpForTotalNodeCount(++$row),
           // $this->createMarkUpForCtWiseNodeCount(++$row),

          $this->createMarkUpForTotalCountOfUrlAliases(++$row),
           $this->createMarkUpForTotalCountOfUrlRedirects(++$row),

           $this->createMarkUpForTotalCountOfMainNavItems(++$row),

           $this->createMarkUpForTotalFileCount(++$row),

          ];
    }

    public function buildTable()
    {
        return $build['table'] = [
            '#type' => 'table',
            '#header' => $this->setHeader(),
            '#rows' => $this->generateTableRows(),
            '#empty' => t('Sorry, nothing to display.'),
          ];
    }
}
